import axios from 'axios'

export default {
    state: {
        photos:[]
        
    },
    mutations: {
        setPhotos(state, payload) {
            state.photos = payload
        }
        
    },
    getters:{  
        getAllPhotos(state) {
            return state.photos
        }     
    },
    actions:{
        fetchPhotos(context){
            axios.get('https://jsonplaceholder.typicode.com/photos?_limit=8')
            .then(response => this.photos = context.commit("setPhotos", response.data))
        }
    }

}